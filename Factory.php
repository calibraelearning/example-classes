<?php

require_once('Lolly.php');

class Factory
{
    public function buildVanilla($units)
    {
        return $this->buildIcecream('Vanilla', $units);
    }

    public function buildChocolate($units)
    {
        return $this->buildIcecream('Chocolate', $units);
    }

    private function buildIcecream($flavour, $units)
    {
        $icecreams = [];
        for($i = 0; $i < $units; $i++)
            $icecreams[] = new Lolly($flavour);

        return $icecreams;
    }
}