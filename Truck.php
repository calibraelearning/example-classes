<?php

class Truck
{
    // need someway of 'holding'?
    private $stock = [];

    public function addStock($icecreams)
    {
        $this->stock = array_merge($this->stock, $icecreams);
    }

    public function vend($flavour)
    {
        foreach($this->stock as $index => $icecream)
            if ($icecream->getFlavour() == $flavour)
                return $this->removeFromStock($index);

        return null;
    }

    // This method is responsible for removing the item from the array of our stock
    // @return The icecream that was removed 
    private function removeFromStock($index)
    {
        $items = array_splice($this->stock, $index, 1);
        return $items[0];
    }

    // Return a report structure 
    public function getStockReport()
    {
        return [
            'total' => count($this->stock),
            'bites' => $this->getTotalBites(),
            'flavours' => $this->getTotalFlavours()
        ];
    }

    private function getTotalBites()
    {
        return 0;
    }

    private function getTotalFlavours()
    { 
        $flavours = [];
        foreach($this->stock as $icecream)
        {
            $flavour = $icecream->getFlavour();
            // have we seen this before?
            if (!in_array($flavour, $flavours))
                $flavours[] = $flavour;
        }

        return $flavours;
    }
}