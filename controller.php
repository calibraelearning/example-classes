<?php

// [include definitions]
include_once('Truck.php');
include_once('Lolly.php');
include_once('Factory.php');

// Get a factory
$factory = new Factory();

// Instruct the factory to build my icecreams
$vanillaIcecreams = $factory->buildVanilla(100);
$chocolateIcecreams = $factory->buildChocolate(200);

// Get a truck
$truck = new Truck();

// Load the truck up with icecreams!
$truck->addStock($vanillaIcecreams);
$truck->addStock($chocolateIcecreams);

// Sell some ice creams!
for ($i = 0; $i < 100; $i++)
    $truck->vend('Vanilla');

$truck->vend('Chocolate');

// do a stock take of the truck!
$report = $truck->getStockReport();
print_r($report);