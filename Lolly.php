<?php

class Lolly
{
    private $flavour;

    public function __construct($flavour)
    {
        $this->flavour = $flavour;
    }

    public function getFlavour()
    {
        return $this->flavour;
    }
}